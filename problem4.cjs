const getCarYears=function(inventory){
    numOfCars=inventory.length;
    let carYearsList=[];
    for(i=0;i<numOfCars;i++){
        carYearsList[i]=inventory[i].car_year;
    }
    return carYearsList;
}
module.exports=getCarYears;
