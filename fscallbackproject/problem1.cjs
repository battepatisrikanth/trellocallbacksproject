const fs=require('fs');
const path=require('path');

function createDirectory(directoryPath,numberOfFiles)
{
    fs.mkdir(directoryPath, (error)=>{

        if(error)
        {
            console.error(error);
        }else{
            console.log("Directory Created SuccessFully");
            createRandomFiles(directoryPath,numberOfFiles);
        }
    });


    function createRandomFiles(directoryPath,numberOfFiles)
    {
        const fileName = `file_${Math.floor(Math.random() * 1000)}.json`;
        const filePath = path.join(directoryPath, fileName);

        const jsonData = {
        randomValue: Math.random(),
        };

        fs.writeFile(filePath,JSON.stringify(jsonData), (error)=>
        {
            if(error)
            {
                console.error(error);
            }
            else{
                    console.log("Files created SuccessFully");
                    // deleteFiles(filePath);
            }
        });
        --numberOfFiles;
        if(numberOfFiles==0)
        {
            return;
        }else{
            createRandomFiles(directoryPath,numberOfFiles);
        }

    }

   
    function deleteFiles(filePath)
    {
        fs.unlink(filePath, (err) => {
            if (err) {
               console.error(err)
            } else {
               console.log("File Successfully Deleted")
            }
          });
    }

}

module.exports=createDirectory;