const sortCarNames=function(inventory){
    numOfCars=inventory.length;
    let sortedNamesList=[];
    for(i=0;i<numOfCars;i++){
        sortedNamesList[i]=inventory[i].car_model;
    }
    return sortedNamesList.sort();
}
module.exports=sortCarNames;
