// Problem 1: Write a function that will return a particular board's information based on the boardID 
// that is passed from the given list of boards in boards.json 
// and then pass control back to the code that called it by using a callback function.

const fs=require('fs');
const path=require('path');
const filePath=path.resolve("boards.json");
function boardInfo(boardID,callback){
    // const resultArray=[];
    setTimeout(()=>{
        fs.readFile(filePath,'utf-8',(error,data)=>{
            if(error){
                callback(error);
            }else{
                data=JSON.parse(data);
                // resultArray.push(data[0])
                // console.log(data);
                let result=data.filter(function (element){
                    return (element.id===boardID)
   
                    })
                callback(null,result);
                // console.log(result);
            }   
        })   
    },2*1000);
}


module.exports=boardInfo;
