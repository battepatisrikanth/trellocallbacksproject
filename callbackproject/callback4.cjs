/* 
    Problem 4: Write a function that will use the previously written functions to get the following information.
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const boardInfo=require("./callback1.cjs");
const cardsInfo=require("./callback3.cjs");
const listInfo=require("./callback2.cjs");

function problem4(boardID,callback){
    setTimeout(()=>{
    boardInfo(boardID,returnThanosBoardInfo);
    function returnThanosBoardInfo(error,data){
        if(error){
            console.error(error);
        }
        else{
            const boardDetails=data;
            listInfo(boardID,listForThanosBoard);
            function listForThanosBoard(error,data){
                if(error){
                    console.error(error);
                }else{
                    const listDetails=data;
                    // console.log(listDetails);
                    const mindListCards=listDetails[0][1].filter((element)=>{
                        return (element.name==='Mind');
                    });
                    // console.log(mindListCards);
                    cardsInfo(mindListCards[0].id,cardsForMindList);
                    function cardsForMindList(error,data){
                        if(error){
                            console.error(error);
                        }else{
                            const cardDetails=data;
                            // console.log(cardDetails);
                            callback(null,boardDetails,listDetails,cardDetails);
                        }
                    }

                
                }
            }
        }
    }
},0)
}

module.exports=problem4;