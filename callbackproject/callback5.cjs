const boardInfo=require("./callback1.cjs");
const cardsInfo=require("./callback3.cjs");
const listInfo=require("./callback2.cjs");

function problem5(boardID,callback){
    setTimeout(()=>{
    boardInfo(boardID,returnThanosBoardInfo);
    function returnThanosBoardInfo(error,data){
        if(error){
            console.error(error);
        }
        else{
            const boardDetails=data;
            listInfo(boardID,listForThanosBoard);
            function listForThanosBoard(error,data){
                if(error){
                    console.error(error);
                }else{
                    const listDetails=data;
                    // console.log(listDetails);
                    const mindListCards=listDetails[0][1].filter((element)=>{
                        return (element.name==='Mind'||element.name==='Space');
                    });
                    // console.log(mindListCards);
                    cardsInfo(mindListCards[0].id,cardsForMindList);
                    function cardsForMindList(error,data){
                        if(error){
                            console.error(error);
                        }else{
                            const cardDetails=data;
                            cardsInfo(mindListCards[1].id,(error,data)=>{
                                if(error){
                                    console.error(error);
                                }else{
                                    console.log(Object.fromEntries(data));
                                }
                            });
                            // console.log(cardDetails);
                            callback(null,boardDetails,listDetails,cardDetails);
                        }
                    }

                
                }
            }
        }
    }
},0)
}

module.exports=problem5;