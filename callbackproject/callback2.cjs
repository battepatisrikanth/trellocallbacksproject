/*  Problem 2: Write a function that will return all lists 
    that belong to a board based on the boardID
    that is passed to it from the given data in lists.json.
    Then pass control back to the code that called it by using a callback function.
*/






const fs=require('fs');
const path=require('path');
const filePath=path.resolve("lists.json");
function listInfo(boardID,callback){
    // const resultArray=[];
    setTimeout(()=>{
        fs.readFile(filePath,'utf-8',(error,data)=>{
            if(error){
                callback(error);
            }else{
                data=JSON.parse(data);
                dataArray=Object.entries(data);
                // resultArray.push(data[0])
                // console.log(data);
                let result=dataArray.filter(function (element){
                    return (element[0]===boardID)
   
                    })
                    // console.log(typeof(data));
                callback(null,result);
                // console.log(result);
            }   
        })   
    },0);
}


module.exports=listInfo;