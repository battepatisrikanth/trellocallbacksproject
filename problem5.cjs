const carsAfterDate=function(inventory){
    numOfCars=inventory.length;
    let carYearsList=[];
    for(i=0;i<numOfCars;i++){
        carYearsList[i]=inventory[i].car_year;
    }
    let afterDateCars=[];
    let j=0;
    for(i=0;i<numOfCars;i++){
        if(carYearsList[i]>2000){
            afterDateCars[j]=carYearsList[i];
            j++;
        }
    }
    return afterDateCars;



}
module.exports=carsAfterDate;
